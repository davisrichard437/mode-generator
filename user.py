# Author: Richard Davis
# user.py - resources for interaction with user

from mode_processing import LilypondNote, LilypondScale, LILYPOND_NOTE_REGEX


def input_ly_note(prompt: str = "Enter note: ") -> LilypondNote:
    """Prompt the user for a lilypond note."""
    
    note = input(prompt)

    while not LILYPOND_NOTE_REGEX.match(note):
        print(f'"{note}" not a valid English lilypond note, please input another.')
        note = input(prompt)

    return note


def input_ly_scale(n) -> LilypondScale:
    """Prompt the user for a lilypond note n times. Return a lilypond scale."""
    
    ly_scale = []

    for i in range(n):
        ly_scale.append(input_ly_note(prompt = f"Enter note {i + 1}: "))

    return ly_scale


def get_num_notes() -> int:
    """Interactively get number of notes in the chord from user."""
    valid_number = False

    while not valid_number:
        num_notes = input("How many notes in the mode (including the first repetition)? ")

        if not num_notes.isdigit():  # Check if input is a number
            print("Input not a valid number, please try again.")
        elif int(num_notes) < 1:          # Check if input number is valid
            print("Please enter a number greater than 0.")
        else:
            valid_number = True

    return int(num_notes)


def print_startup_message() -> None:
    """Prints startup message"""
    
    print("Mode Generator")
    print("Copyright (c) Richard Davis, 2022")
    print("This script generates musical modes using English Lilypond notes.\n")
