# Author: Richard Davis
# resources.py - resources for mode generation and printing

""" List of resources for mode operations.

Aliases the following types:
- LilypondNote: a string, in the format of LILYPOND_NOTE_REGEX
- MIDINote: a MIDI pitch integer (0-127)
- Interval: an interval in semitones
- IntervalScale: a list of Intervals that define a scale
- LilypondScale: a list of LilypondNotes that define a scale (including the
first repeated note)
- MIDIScale: a list of MIDINotes that define a scale (including the first
repeated note)
"""

import re

LilypondNote = str
MIDINote = int
Interval = int
IntervalScale = list[Interval]
LilypondScale = list[LilypondNote]
MIDIScale = list[MIDINote]

# This regex supports english-style lilypond accidentals (f = flat, s = sharp),
# and no articulations, durations, etc, as they are not processed by this script
LILYPOND_NOTE_REGEX = re.compile("^[a-g][f|s]*[,|']*$")

ACCIDENTALS = [
    "s",
    "f",
]

TRANSPOSITIONS = [
    "'",
    ",",
]

UNTRANSPOSED_MIDI_NOTES = {
    48: "c",
    49: "cs",
    50: "d",
    51: "ef",
    52: "e",
    53: "f",
    54: "fs",
    55: "g",
    56: "af",
    57: "a",
    58: "bf",
    59: "b",
}

# Reverse key value pairs in UNTRANSPOSED_MIDI_NOTES to get pairs of
# "ly_note: midi_note"
UNTRANSPOSED_LY_NOTES = {v: k for k, v in UNTRANSPOSED_MIDI_NOTES.items()}

PITCH_CLASS_INTS = {k % 12: v for k, v in UNTRANSPOSED_MIDI_NOTES.items()}


def parse_ly_note(ly_note: LilypondNote) -> list[str]:
    """ Parse lilypond note and return list of components.
    >>> parse_ly_note("cs'")
    ["c", "s", "'"]
    """

    if not LILYPOND_NOTE_REGEX.match(ly_note):
        raise ValueError(f"'{ly_note}' not a valid English Lilypond note.")

    components_list = ["", "", ""]

    # Add note name
    components_list[0] = ly_note[0]

    for c in ly_note[1:]:
        if c in ACCIDENTALS:
            components_list[1] += c
        elif c in TRANSPOSITIONS:
            components_list[2] += c

    return components_list


def ly_to_midi(ly_note: LilypondNote) -> MIDINote:
    """ Translate lilypond note to midi note.
    >>> ly_to_midi("cs'")
    61
    """

    components_list = parse_ly_note(ly_note)

    # Initialize midi_note with its absolute, untransposed midi note value
    midi_note = UNTRANSPOSED_LY_NOTES[components_list[0]]

    # Add 1 for every sharp present
    midi_note += 1 * components_list[1].count(ACCIDENTALS[0])
    # Subtract 1 for every flat present
    midi_note -= 1 * components_list[1].count(ACCIDENTALS[1])

    # Add 12 for every ' present
    midi_note += 12 * components_list[2].count(TRANSPOSITIONS[0])
    # Subtract 12 for every , present
    midi_note -= 12 * components_list[2].count(TRANSPOSITIONS[1])

    return midi_note


def midi_to_ly(midi_note: MIDINote) -> LilypondNote:
    """ Translate midi note to lilypond note.
    >>> midi_to_ly(61)
    "cs'"
    """

    if midi_note < 0:
        raise ValueError(f"{midi_note} is not a valid MIDI note.")

    ly_note = ""

    pitch_class_int = midi_note % 12
    pitch_class = PITCH_CLASS_INTS[pitch_class_int]
    ly_note += pitch_class

    untransposed_midi_pitch_int = UNTRANSPOSED_LY_NOTES[pitch_class]

    # Get number of octaves transposed
    transposition = (midi_note - untransposed_midi_pitch_int) // 12

    # Add transposition character according to the number of times transposed
    if transposition > 0:
        ly_note += 1 * transposition * TRANSPOSITIONS[0]
    elif transposition < 0:
        ly_note += -1 * transposition * TRANSPOSITIONS[1]

    return ly_note


def scale_ly_to_midi(ly_scale: LilypondScale) -> MIDIScale:
    """ Translate lilypond scale to midi scale.
    >>> scale_ly_to_midi(["bf,", "cs", "d", "e", "f", "gs", "a"])
    [46, 49, 50, 52, 53, 56, 57]
    """

    return list(map(ly_to_midi, ly_scale))


def scale_midi_to_ly(midi_scale: MIDIScale) -> LilypondScale:
    """ Translate midi scale to lilypond scale.
    >>> scale_midi_to_ly([46, 49, 50, 52, 53, 56, 57])
    ["bf,", "cs", "d", "e", "f", "af", "a"]
    """

    return list(map(midi_to_ly, midi_scale))


def get_interval(low_note: MIDINote, high_note: MIDINote) -> Interval:
    """ Calculate the interval in semitones between two midi notes.
    >>> get_interval(46, 57)
    11
    """

    return high_note - low_note


def scale_midi_to_interval(midi_scale: MIDIScale) -> IntervalScale:
    """ Translate midi scale to list of adjacent intervals.
    >>> midi_scale_to_adj_intervals([46, 49, 50, 52, 53, 56, 57])
    [3, 1, 2, 1, 3, 1]
    """

    return [get_interval(midi_scale[i], midi_scale[i+1]) for i in range(len(midi_scale)-1)]


def scale_ly_to_interval(ly_scale: LilypondScale) -> IntervalScale:
    """ Translate lilypond scale to interval scale.
    >>> scale_ly_to_interval(["bf,", "cs", "d", "e", "f", "gs", "a"])
    [3, 1, 2, 1, 3, 1]
    """

    return scale_midi_to_interval(scale_ly_to_midi(ly_scale))


def expand_interval_scale(
        interval_scale: IntervalScale,
        low_midi_note: MIDINote,
        high_midi_note: MIDINote,
        starting_midi_note: MIDINote,
) -> MIDIScale:
    """ Expand midi_scale to occupy pitch space between low_midi_note and
    high_midi_note, starting on starting_midi_note.
    >>> expand_interval_scale([3, 1, 2, 1, 3, 1], 34, 60, 46)
    [34, 35, 38, 39, 41, 42, 45, 46, 49, 50, 52, 53, 56, 57, 60]
    """

    if not low_midi_note <= starting_midi_note <= high_midi_note:
        raise ValueError(f"{starting_midi_note} not within range between {low_midi_note} and {high_midi_note}.")

    # Build the upper part of the midi scale
    midi_scale_upper_segment = [starting_midi_note]

    current_pitch = starting_midi_note

    # TODO: simplify?
    while current_pitch <= high_midi_note:
        for i in interval_scale:
            note_to_add = midi_scale_upper_segment[-1] + i

            if note_to_add <= high_midi_note:
                midi_scale_upper_segment.append(note_to_add)
                current_pitch = midi_scale_upper_segment[-1]
            else:
                current_pitch = note_to_add
                # stop iterating if out of bounds
                break

    # Build the lower part of the midi scale
    midi_scale_lower_segment = []

    current_pitch = starting_midi_note

    while current_pitch >= low_midi_note:
        for i in interval_scale[::-1]:
            note_to_add = current_pitch - i

            if note_to_add >= low_midi_note:
                # prepend
                midi_scale_lower_segment.insert(0, note_to_add)
                current_pitch = midi_scale_lower_segment[0]
            else:
                current_pitch = note_to_add
                # stop iterating if out of bounds
                break

    return midi_scale_lower_segment + midi_scale_upper_segment


def expand_ly_scale(
        ly_scale: LilypondScale,
        low_ly_note: LilypondNote,
        high_ly_note: LilypondNote,
        starting_ly_note: LilypondNote,
) -> LilypondScale:
    """ Wraps `expand_midi_scale` by translating lilypond objects to midi.
    >>> expand_ly_scale(["bf,", "cs", "d", "e", "f", "gs", "a"], "bf,,", "c'", "bf,")
    ['bf,,', 'b,,', 'd,', 'ef,', 'f,', 'fs,', 'a,', 'bf,', 'cs', 'd', 'e', 'f', 'af', 'a', "c'"]
    """

    interval_scale = scale_ly_to_interval(ly_scale)

    low_midi_note = ly_to_midi(low_ly_note)
    high_midi_note = ly_to_midi(high_ly_note)
    starting_midi_note = ly_to_midi(starting_ly_note)

    midi_scale = expand_interval_scale(
        interval_scale,
        low_midi_note,
        high_midi_note,
        starting_midi_note
    )

    return scale_midi_to_ly(midi_scale)


if __name__ == "__main__":
    # Testing code for functions that return without user input
    print("parse_ly_note() test passed:", parse_ly_note("cs'") == ["c", "s", "'"])
    print("ly_to_midi() test passed:", ly_to_midi("cs'") == 61)
    print("midi_to_ly() test passed:", midi_to_ly(61) == "cs'")
    print("scale_ly_to_midi() test passed:", scale_ly_to_midi(["bf,", "cs", "d", "e", "f", "gs", "a"]) == [46, 49, 50, 52, 53, 56, 57])
    print("scale_midi_to_ly() test passed:", scale_midi_to_ly([46, 49, 50, 52, 53, 56, 57]) == ["bf,", "cs", "d", "e", "f", "af", "a"])
    print("get_interval() test passed:", get_interval(46, 57) == 11)
    print("midi_scale_to_adj_intervals() test passed:", scale_midi_to_interval([46, 49, 50, 52, 53, 56, 57]) == [3, 1, 2, 1, 3, 1])
    print("expand_interval_scale() test passed:", expand_interval_scale([3, 1, 2, 1, 3, 1], 34, 60, 46) == [34, 35, 38, 39, 41, 42, 45, 46, 49, 50, 52, 53, 56, 57, 60])
    print("expand_ly_scale() test passed:", expand_ly_scale(["bf,", "cs", "d", "e", "f", "gs", "a"], "bf,,", "c'", "bf,") == ['bf,,', 'b,,', 'd,', 'ef,', 'f,', 'fs,', 'a,', 'bf,', 'cs', 'd', 'e', 'f', 'af', 'a', "c'"])
