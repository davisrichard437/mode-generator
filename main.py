#!/usr/bin/env python3
# Author: Richard Davis
# main.py - main script for mode processing

import mode_processing, filesystem, user


def main() -> None:
    """The main functionality of the script."""
    user.print_startup_message()

    num_notes = user.get_num_notes()

    print("Enter the mode:")
    ly_scale = user.input_ly_scale(num_notes)

    low_ly_note = user.input_ly_note(prompt = "Input low note: ")
    high_ly_note = user.input_ly_note(prompt = "Input high note: ")
    starting_ly_note = user.input_ly_note(prompt = "Input starting note: ")

    expanded_ly_scale = mode_processing.expand_ly_scale(
        ly_scale,
        low_ly_note,
        high_ly_note,
        starting_ly_note,
    )

    file_number = filesystem.increment_filename()
    filesystem.print_scale_to_lilypond(expanded_ly_scale, file_number)


try:
    main()
except KeyboardInterrupt:
    print("\nCtrl-c (keyboard interrupt) pressed, exiting.")
    quit()
