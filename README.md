# Mode Generator

Simple python script to expand modes across a specified range.

## Dependencies

- Python 3.9 or later
- Lilypond

## Usage

Clone the repository:

```
git clone https://gitlab.com/davisrichard437/mode-generator.git
cd mode-generator
```

Run the mode generator

    ./main.py

Output can be found in the `mode-generator/out` directory.
